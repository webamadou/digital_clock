//====================================================================================================================

const clock = document.getElementById('clock');

let date = null;

setInterval(() => {
  date = new Date();

  clock.innerHTML = `${date.getDay()} - ${date.getUTCMonth()} - ${date.getFullYear()}<br> - <br>${date.getHours()} : ${date.getMinutes()} : ${date.getSeconds()}`;
},1000);
